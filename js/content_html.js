var link = document.getElementsByTagName('div');
var ajax = new XMLHttpRequest();
var main = document.getElementById("tpl__content");
var mainInner = document.getElementsByClassName('tpl-container-inner');
var inner = document.createElement('div');

   // for (var i = 0; i < link.length; i++) {
   //     link[i].onclick = function () {
   //
   //         $('main').load(this.id + '.html .tpl-container');
   //     };
   // };

/*    for (var i = 0; i < link.length; i++) {
        link[i].onclick = function () {
            ajax.open("GET", this.id + ".html", true);
            ajax.send();
            ajax.onload = function() {
                inner.innerHTML = ajax.responseText;
                main.innerHTML = inner.querySelector('tpl-container')[0].innerHTML;
                document.body.insertBefore(main, document.body.childNodes[0]);
            };

//            ajax.onload = function(e) {
//                var div = document.getElementById("demo");
//                div.innerHTML = ajax.responseText;
//                document.body.insertBefore(div, document.body.childNodes[0]);
//            };
        };
    };*/

function httpGet(url) {

    return new Promise(function(resolve, reject) {

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status == 200) {
                resolve(
                    this.response
                );
            } else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error("Network Error"));
        };

        xhr.send();
    });

}

for (var i = 0; i < link.length; i++) {
    link[i].onclick = function () {
//            'use strict';

        // Метод fetch

        fetch(this.id + ".html").then(res => {
            inner.innerHTML = res;
            console.log(typeof inner);
            main.innerHTML = inner.querySelector('.tpl-container').innerHTML;
        }).catch (function (err) {
            console.log(err)
        });

        // Обертка
        /*httpGet(this.id + ".html")
            .then(response => {
                let pageHTML = response;
                inner.innerHTML = pageHTML;
                main.innerHTML = inner.querySelector('.tpl-container').innerHTML;
            })*/

    }
}
